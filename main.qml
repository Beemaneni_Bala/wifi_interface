import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.VirtualKeyboard 2.0
import QtQuick.Controls 2.1

import MyScript 1.0

Window {
    id: root


    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")


    property string hintText: "Choose Test: " + "\n" +
                              "1 - Select Frequency" + " \n" +
                              "2 - Select Frequency" + " \n" +
                              "3 - Select Frequency" + " \n" +
                              "4 - Select Frequency" + " \n" +
                              "5 - Select Frequency" + " \n" +
                              "6 - Select Frequency" + " \n" +
                              "7 - Select Frequency" + " \n" +
                              "8 - Select Frequency" + " \n" +
                              "9 - Select Frequency" ;


    property var testName : [
        "0 - NULL",
        "1 - Select Frequency",
        "2 - Select Frequency",
        "3 - Select Frequency",
        "4 - Select Frequency",
        "5 - Select Frequency",
        "6 - Select Frequency",
        "7 - Select Frequency",
        "8 - Select Frequency",
        "9 - Select Frequency"
    ]


    Text{
        id: userInput
        anchors{left: parent.left; leftMargin: 30;bottom: parent.bottom; bottomMargin: 30 }
        text: hintText
        width: 300
        wrapMode: Text.WordWrap
        font.pixelSize: 22
        font.family: "Roboto Regular"
    }


    property var scriptClass: _myScriptClass

    TextInput {
        id: textEdit
        text: qsTr("Input Number between 1 and 10")
        verticalAlignment: Text.AlignVCenter
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 20
        inputMethodHints: Qt.ImhDigitsOnly
        width: 300
        onFocusChanged: {
            if(focus){
                text=""
                runningsTest.text=""
            }
        }

        onTextChanged: {
            var taskN = parseInt(textEdit.text)
            if(taskN > 0 && taskN < 10){
                runButton.enabled=true
            }
            else{
                runButton.enabled=false
            }
        }

        Rectangle {
            id: fillColor
            anchors.fill: parent
            anchors.margins: -10
            color: "transparent"
            border.width: 1
        }
    }

    Text{
        id: runningsTest
        anchors{top: runButton.bottom;topMargin: 20}
        anchors.horizontalCenter: textEdit.horizontalCenter
        font.pixelSize: 22
        font.family: "Roboto Regular"
        color: "blue"
    }



    Button{
        id: runButton
        anchors{top: textEdit.bottom;topMargin: 20}
        anchors.horizontalCenter: textEdit.horizontalCenter
        text: "RUN"
        onClicked: {
            scriptClass.invokeScript(parseInt(textEdit.text))
            runningsTest.text = "Selected Test is: " + testName[parseInt(textEdit.text)]
        }
    }

    MyScript{
        id: _myScriptClass
    }

    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: root.height
        width: root.width

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: root.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }

}
