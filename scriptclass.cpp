#include "scriptclass.h"
#include <QDebug>
#include <QCoreApplication>
#include <QVariantList>
#include <QTimer>
#include <QEventLoop>

ScriptClass::ScriptClass(QObject *parent) : QObject(parent)
{
   process = new QProcess(this);
   connect(process,SIGNAL(readyReadStandardOutput()),this,SLOT(readProcessData()));
   connect(process,SIGNAL(finished(int,QProcess::ExitStatus)),this,SLOT(processPythonSlot(int,QProcess::ExitStatus)));
   connect(process,SIGNAL(errorOccurred(QProcess::ProcessError)),this,SLOT(captureErrorProcess(QProcess::ProcessError)));

}


void ScriptClass::invokeScript(QVariantList arguments ){

    QStringList params;
    QString path = QCoreApplication::applicationDirPath() + "/wifi_test.sh";

    params << path;
    for(int i=0;i<arguments.length();i++){
        params << arguments.at(i).toString();
    }
    qDebug()<<"Inputs are: "<<params;
    process->start("/bin/sh", params);

}


void ScriptClass::readProcessData(){
    qDebug()<<"Reading Dataaaaa: ";
    dataRead(process->readAll());
}
void ScriptClass::processPythonSlot(int value, QProcess::ExitStatus procState){
    qDebug() << " The output from process Slot is: " <<value <<procState << endl;
    switch (procState) {
    case QProcess::NormalExit:
        qDebug()<<"Process Completed With Normal Exit "<<procState;
        processComplete(1);
        break;
    case QProcess::CrashExit:
        qDebug()<<"Process Completed With Crash Exit "<<procState;
        processComplete(2);
        break;
    default:
        break;
    }
}
void ScriptClass::captureErrorProcess(QProcess::ProcessError errorProc){
    qDebug() << " Error Occured: " <<errorProc << endl;
    switch (errorProc) {
    case 0:
        qDebug()<<"EMiting the signal from Error slot "<<errorProc;
        break;
    case 1:

        break;
    case 2:

        break;
    default:
        break;
    }
}
