import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.VirtualKeyboard 2.0
import QtQuick.Controls 2.1
import QtQuick.Controls 2.2

import MyScript 1.0

Window {
    id: root

    visible: true
    width: 1024
    height: 600
    title: qsTr("Hello World")

    property var scriptClass: _myScriptClass

    property string antenna : (antennaA.checked ? "A" : (antennaB.checked ? "B" : "AB"))
    property string txnMode : "TX"
    property string bandModeIp : "5G"
    property string channelIp : "36"
    property string bandWidth : "20M"

    property string dataRateIp: "108"
    property int powerIndexAIp : 0
    property int powerIndexBIp : 0
    property string txModeToneIp : "CONTINUOUS_TX"

    Column {
        id: columnData1
        anchors{left: parent.left;leftMargin: 30; top :parent.top;topMargin: 50 }
        spacing: 30

        Row{
            id: antennaeData
            Text{
                id: antennaeText
                width: 120
                text: "Antenna"
            }
            RadioButton{
                id: antennaA
                anchors.verticalCenter: antennaeText.verticalCenter
                text: "A"
                checked: true
            }
            RadioButton{
                id: antennaB
                anchors.verticalCenter: antennaeText.verticalCenter
                text: "B"
            }
            RadioButton{
                id: antennaAB
                anchors.verticalCenter: antennaeText.verticalCenter
                text: "AB"
            }
        }


        Row{
            Text{
                id: channelText
                width: 120
                text: "Channel"
            }
            Rectangle{
                anchors.verticalCenter: channelText.verticalCenter
                width: 100
                height: 50
                border.color: "gray"
                border.width: 3
                TextInput{
                    anchors.centerIn: parent
                    wrapMode: Text.WordWrap
                    clip: true
                    anchors.fill: parent
                    inputMethodHints: Qt.ImhDigitsOnly
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    color: "blue"
                    text: "36"
                    font.family: "Roboto Regular"
                    font.pixelSize: 22
                    validator: inputValidator

                    onTextChanged: {
                        channelIp=text
                    }
                }
            }
        }

        Row{
            Text{
                id: txMode
                text: "Mode"
                width: 120
            }

            ComboBox{
                model: ["TX","RX"]
                anchors.verticalCenter: txMode.verticalCenter
                onActivated: {
                    console.log("Band Mode is: ",model[index])
                    txnMode=model[index]
                }
            }
        }
        Row{
            Text{
                id: bandWidthText
                width: 120
                text: "Bandwidth"
            }

            ComboBox{
                id: bandWidthBox
                model: ["20M","40M","80M"]
                anchors.verticalCenter: bandWidthText.verticalCenter
                onActivated: {
                    console.log("Bandwith is: ",model[index])
                    bandWidth=model[index]
                }
            }
        }

        Row{
            Text{
                id: bandMode
                text: "Band"
                width: 120
            }

            ComboBox{
                model: ["5G","2G"]
                anchors.verticalCenter: bandMode.verticalCenter
                onActivated: {
                    console.log("Band Mode is: ",model[index])
                    bandModeIp=model[index]
                    channelIp.validator=inputValidator
                }
            }
        }



    }


    MyScript{
        id: _myScriptClass

        onDataRead: {
            outputProc.text += output + "\n"
        }

        onProcessComplete: {
            if(procOp===1){
                outputProc.text += "Process Completed Successfully"
                runButton.enabled=true
            }
            else if(procOp===1){
                outputProc.text += "Process Crashed"
                runButton.enabled=true
            }
        }
    }

    IntValidator{
        id: inputValidator
        bottom: bandModeIp==="5G" ? 36 : 1
        locale: Qt.I
        top: bandModeIp==="5G" ? 165 : 14
    }

    Column {
        id: columnData2
        anchors{left: columnData1.right;leftMargin: 50; top :parent.top;topMargin: 50 }
        spacing: 30

        Row{
            Text{
                id: dataRate
                text: "Data Rate"
                width: 120
            }
            Rectangle{
                anchors.verticalCenter: dataRate.verticalCenter
                width: 100
                height: 50
                border.color: "gray"
                border.width: 3
                TextInput{
                    anchors.centerIn: parent
                    wrapMode: Text.WordWrap
                    clip: true
                    anchors.fill: parent
                    inputMethodHints: Qt.ImhDigitsOnly
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    color: "blue"
                    font.family: "Roboto Regular"
                    text: "108"
                    font.pixelSize: 22
                    validator: IntValidator{bottom: 0;
                        top: 10000;
                    }
                    onTextChanged: {
                        dataRateIp = text
                    }
                }
            }
        }
        Row{
            Text{
                id: powerIndexA
                width: 120
                text: "Power Index A"
            }

            Rectangle{
                anchors.verticalCenter: powerIndexA.verticalCenter
                width: 100
                height: 50
                border.color: "gray"
                border.width: 3
                TextInput{
                    anchors.centerIn: parent
                    wrapMode: Text.WordWrap
                    clip: true
                    anchors.fill: parent
                    inputMethodHints: Qt.ImhDigitsOnly
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    color: "blue"
                    font.family: "Roboto Regular"
                    font.pixelSize: 22
                    text: "0"
                    validator: IntValidator{bottom: 0; top: 63;}

                    onTextChanged: {
                        powerIndexAIp=text
                    }
                }
            }
        }
        Row{
            Text{
                id: powerIndexB
                width: 120
                text: "Power Index B"
            }

            Rectangle{
                anchors.verticalCenter: powerIndexB.verticalCenter
                width: 100
                height: 50
                border.color: "gray"
                border.width: 3
                TextInput{
                    anchors.centerIn: parent
                    wrapMode: Text.WordWrap
                    clip: true
                    anchors.fill: parent
                    inputMethodHints: Qt.ImhDigitsOnly
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    color: "blue"
                    font.family: "Roboto Regular"
                    font.pixelSize: 22
                    validator: IntValidator{bottom: 0; top: 63;}
                    text: "0"
                    onTextChanged: {
                        powerIndexBIp =text
                    }
                }
            }
        }
        Row{
            Text{
                id: txModetone
                text: "TX Mode"
                width: 120
            }

            ComboBox{
                model: ["CONTINUOUS_TX","TONE_TX"]
                anchors.verticalCenter: txModetone.verticalCenter
                width: 180
                onActivated: {
                    console.log("Band Mode is: ",model[index])
                    txModeToneIp=model[index]
                }
            }
        }
    }



    TextArea{
        id: displayText
        anchors{left: columnData2.right;leftMargin: 20; top :parent.top;topMargin: 50 }
        visible: false
        text: "The Inputs selected are as belows: \n" +
              "Mode\t\t:  "+ txnMode    + "\n" +
              "Channel\t\t:  "+ channelIp    + "\n" +
              "Antenna\t\t:  "+ antenna    + "\n" +
              "Band\t\t: "+ bandModeIp + "\n" +
              "Band Width\t:  "+ bandWidth  + "\n" +
              "Data Rate\t\t:  "+ dataRateIp + "\n" +
              "Power Index A\t:  "+ powerIndexAIp + "\n" +
              "Power Index B\t:  "+ powerIndexBIp  + "\n" +
              "TX Mode\t\t:  "+ txModeToneIp + "\n" ;
    }

    Button{
        id: runButton
        text: "Run"
        font.pixelSize: 22
        font.family: "Roboto Regular"
        anchors{left: parent.left;leftMargin: 50;bottom: parent.bottom;bottomMargin: 50}
        onClicked: {
            enabled=false
            displayText.visible=true
            outputProc.text=""
            var data=[]
            data.push(txnMode)
            data.push(channelIp)
            data.push(bandWidth)
            data.push(dataRateIp)
            data.push("SHORT")
            data.push(powerIndexAIp)
            data.push(powerIndexBIp)
            data.push(txModeToneIp)
            data.push(bandModeIp)
            data.push(antenna)
            scriptClass.invokeScript(data)
        }
    }

    ScrollView{
        id: view
        anchors{left: columnData1.right;right: parent.right;
            top: columnData2.bottom;bottom: parent.bottom;bottomMargin: 20}

        TextArea{
            id: outputProc
             anchors.fill: parent
             readOnly: true
             color: "blue"
             wrapMode: Text.WordWrap
        }
    }

    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: root.height
        width: root.width

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: root.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }
}


