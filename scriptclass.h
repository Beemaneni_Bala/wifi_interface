#ifndef SCRIPTCLASS_H
#define SCRIPTCLASS_H

#include <QObject>
#include <QVariantList>
#include <QProcess>

class ScriptClass : public QObject
{
    Q_OBJECT
public:
    explicit ScriptClass(QObject *parent = nullptr);
    Q_INVOKABLE void invokeScript(QVariantList);
signals:
    void dataRead(QString output);
    void processComplete(int procOp);
public slots:
    void readProcessData();

    void processPythonSlot(int value, QProcess::ExitStatus procState);
    void captureErrorProcess(QProcess::ProcessError errorProc);
private:
      QProcess *process;
};

#endif // SCRIPTCLASS_H
