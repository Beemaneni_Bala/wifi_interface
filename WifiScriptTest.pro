# Add more folders to ship with the application, here

folder_01.source = Scripts
folder_01.target = Wifi

DEPLOYMENTFOLDERS = folder_01



CONFIG += copy_files

COPY_FILES += Scripts/*
target.path ="/home/root"

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp \
    scriptclass.cpp

RESOURCES += qml.qrc
TARGET = Variscite

TEMPLATE=app

INSTALLS += target
#copydata.commands = $(COPY_DIR) $$PWD/wifi_test.sh $$./
#first.depends = $(first) copydata
#export(first.depends)
#export(copydata.commands)
#QMAKE_EXTRA_TARGETS += first copydata

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

CONFIG += c++11
CONFIG += link_pkgconfig
CONFIG += disable-desktop
# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /home/root
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    scriptclass.h

DISTFILES += \
    wifi_test.sh \
    copy_files.prf

