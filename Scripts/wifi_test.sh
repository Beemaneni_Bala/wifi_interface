#!/bin/bash
#Arguments TXorRX /Channel/Bandwidth/Data Rate /Preamble / PowerA/PowerB/mode of transmit or receive/Band / Antennae
if [ "$#" -ne 10 ]; then
    echo "Illegal number of parameters"
    exit 1
fi
TX_RX=$1
Channel=$2
Bandwidth=$3
DataRate=$4
Preamble=$5
PowerGainA=$6
PowerGainB=$7
TX_Mode=$8
Band=$9
Antennae=$10
echo "Wifi will be tested with TX_RX with $TX_RX, Channel=$Channel, Bandwidth=$Bandwidth,DataRate=$DataRate,Preamble=$Preamble, \
      PowerGainAB=$PowerGainA/$PowerGainB and Traffic=$TX_Mode and Band=$Band and Antennae=$Antennae"
#Validating Input Parameters
if !(([ $Band = "5G" ] && ([ $Channel -ge 36 ] && [ $Channel -le 165 ])) || ([ $Band = "2G" ] && ([ $Channel -ge 1 ] && [ $Channel -le 14 ])) \
     && ([ $Band = "5G" ] || [ $Band = "2G" ]) && ([ $TX_RX = "TX" ] || [ $TX_RX = "RX" ]) \
     && ([ $Bandwidth = "20M" ] || [ $Bandwidth = "40M" ] || [ $Bandwidth = "80M" ]) \
     && ([ $PowerGainA -ge 0 ] && [ $PowerGainA -le 63 ] ) && ([ $PowerGainB -ge 0 ] && [ $PowerGainB -le 63 ] ) \
     && ([ $Antennae = "A" ] || [ $Antennae = "B" ] || [ $Antennae = "AB" ]) \
     &&	([ $TX_Mode = "CONTINUOUS_TX" ] || [ $TX_Mode = "TONE_TX" ]) \
     && ([ $Preamble = "SHORT" ] || [ $Preamble = "LONG" ])); then
	echo "Non Valid Channel Param"
	exit 2
fi


#wlan picking
if [ $Band = "5G" ]; then
	wlan="wlan1"	

	wlan="wlan0"
fi

#Bandwidth picking
if [ $Bandwidth = "20M" ]; then
	Bandwidth_arg=0	
elif [ $Bandwidth = "40M" ]; then
	Bandwidth_arg=1	

else
	Bandwidth_arg=2
fi

#Antennae Picking
if [ $Antennae = "A" ]; then
	Antennae_arg=1	
elif [ $Antennae = "B" ]; then
	Antennae_arg=2	
else
	Antennae_arg=3
fi

#Preamble Picking
if [ $Preamble = "SHORT" ]; then
	Preamble_arg=0	
else
	Preamble_arg=1
fi

#TxMode Picking
if [ $TX_Mode = "CONTINUOUS_TX" ]; then
	TX_Mode_arg=0	
else
	TX_Mode_arg=2
fi
echo "Wlan is $wlan, bandwidth argument=$Bandwidth_arg, antennae argument=$Antennae_arg, preamble=$Preamble_arg, Txmode=$TX_Mode_arg"
echo "rtwpriv mp_ctx stop"
echo "rtwpriv mp_stop"
echo "ifconfig wlan0 down"
echo "ifconfig wlan1 down"
echo "ifconfig $wlan up"
sleep 1
echo "rtwpriv $wlan mp_start"
sleep 1
#TxMode Picking
if [ $TX_RX = "TX" ]; then
	echo "rtwpriv $wlan mp_tx ch=$Channel,bw=$Bandwidth_arg,rate=$DataRate,pwr=$PowerGainA,ant=$Antennae_arg,tx=$TX_Mode_arg"
else
	echo "rtwpriv $wlan mp_rx ch=$Channel,bw=$Bandwidth_arg,ant=$Antennae_arg"
fi
exit 3


  
