#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "scriptclass.h"
int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QGuiApplication app(argc, argv);


    qmlRegisterType<ScriptClass>("MyScript",1,0,"MyScript");
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/WifiTestPage.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
